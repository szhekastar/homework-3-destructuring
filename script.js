// Задание 1

// const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
//   const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];
//   const clients = [...clients1, ...clients2];
  
//   function unique (arr) {
//     let result = [];
//     for (let str of arr) {
//       if(!result.includes(str)) {
//         result.push (str)
//       }
//     }
//     return result;
//   }
//   let res =  unique(clients);
//   console.log(res)


//  Задание 2

// const characters = [
//   {
//     name: "Елена",
//     lastName: "Гилберт",
//     age: 17, 
//     gender: "woman",
//     status: "human"
//   },
//   {
//     name: "Кэролайн",
//     lastName: "Форбс",
//     age: 17,
//     gender: "woman",
//     status: "human"
//   },
//   {
//     name: "Аларик",
//     lastName: "Зальцман",
//     age: 31,
//     gender: "man",
//     status: "human"
//   },
//   {
//     name: "Дэймон",
//     lastName: "Сальваторе",
//     age: 156,
//     gender: "man",
//     status: "vampire"
//   },
//   {
//     name: "Ребекка",
//     lastName: "Майклсон",
//     age: 1089,
//     gender: "woman",
//     status: "vempire"
//   },
//   {
//     name: "Клаус",
//     lastName: "Майклсон",
//     age: 1093,
//     gender: "man",
//     status: "vampire"
//   }
// ];

// function sortObj (arr) {
//   const result = [];
//   for (let index in arr) {
//     let obj = arr[index];
//     let {name , lastName , age} = obj;
//     const persone = {
//       name: name,
//       lastName: lastName,
//       age:age,
//     }
//     result.push(persone)
//   }
// return result;
// }
// const charactersShortInfoResult = sortObj(characters);
// const charactersShortInfoResult2 = sortObj2(characters);

// console.log('charactersShortInfoResult ->', charactersShortInfoResult);
// console.log('charactersShortInfoResult2 ->', charactersShortInfoResult2)

// function sortObj2(charactersInput) {
//   return charactersInput.map(character => {
//     const { name, lastName, age } = character;
//     return {
//       name,
//       lastName,
//       age
//     };
//   })
// }



//  Задание 3

// const user1 = {
//   name: "John",
//   years: 30
// };
// const {name, years : age, isAdmin = false} = user1;
// console.log(name)
// console.log(age)
// console.log(isAdmin)



//  Задание 4

// const satoshi2020 = {
//   name: 'Nick',
//   surname: 'Sabo',
//   age: 51,
//   country: 'Japan',
//   birth: '1979-08-21',
//   location: {
//     lat: 38.869422, 
//     lng: 139.876632
//   }
// }

// const satoshi2019 = {
//   name: 'Dorian',
//   surname: 'Nakamoto',
//   age: 44,
//   hidden: true,
//   country: 'USA',
//   wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
//   browser: 'Chrome'
// }

// const satoshi2018 = {
//   name: 'Satoshi',
//   surname: 'Nakamoto', 
//   technology: 'Bitcoin',
//   country: 'Japan',
//   browser: 'Tor',
//   birth: '1975-04-05'
// }


// const arrObj = [satoshi2018,satoshi2019,satoshi2020 ];

// function parseSatoshiData (arr) {
//   const result = {};
//   arr.forEach(element => {
//     const {name ,surname,age,country,birth,location,wallet,browser,technology} = element;
//     result['name'] = name || result.name;
//     result['surname'] = surname || result.surname;
//     result['age'] = age || result.age;
//     result['country'] = country || result.country;
//     result['birth'] = birth || result.birth;
//     result['location'] = location || result.location;
//     result['wallet'] = wallet || result.wallet;
//     result['browser'] = browser || result.browser;
//     result['technology'] = technology || result.technology;
//   });
//   return result;
// }
// let fullProfile = parseSatoshiData(arrObj);
// console.log(fullProfile);

// console.log(' ->', {...satoshi2018, ...satoshi2019, ...satoshi2020});



//  Задание 5


// const books = [{
//   name: 'Harry Potter',
//   author: 'J.K. Rowling'
// }, {
//   name: 'Lord of the rings',
//   author: 'J.R.R. Tolkien'
// }, {
//   name: 'The witcher',
//   author: 'Andrzej Sapkowski'
// }];

// const bookToAdd = {
//   name: 'Game of thrones',
//   author: 'George R. R. Martin'
// }

// const booksResult = [...books ,bookToAdd];


//  Задание 6

// const employee = {
//   name: 'Vitalii',
//   surname: 'Klichko'
// }

// const resultEmployee = {...employee, age:45,salary:200};
// console.log(employee);
// console.log(resultEmployee);





//  Задание 7


// const array = ['value', () => 'showValue'];
// const [ value,showValue ] = array;


// alert(value); 
// alert(showValue());